NOECHO ?= @
BUILDDIR ?= build
MKDIR = $(NOECHO)mkdir -p $(dir $@)
MODULE ?= hw_02
SRC = main.cpp
LIBNAME := calculator
LIBTARGET := lib$(LIBNAME).so
CFLAGS := -Wall -Werror

all: $(BUILDDIR)/$(MODULE)

$(BUILDDIR)/$(LIBTARGET):
	$(MKDIR)
	$(NOECHO)$(CXX) $(CFLAGS) -c $(LIBNAME)/$(LIBNAME).cpp -fpic -o $(BUILDDIR)/$(LIBNAME).o
	$(NOECHO)$(CXX) -shared $(BUILDDIR)/$(LIBNAME).o -o $@

$(BUILDDIR)/$(MODULE): lib
	$(MKDIR)
	$(NOECHO)$(CXX) $(SRC) $(CFLAGS) -l$(LIBNAME) -L$(BUILDDIR) -o $@

lib: $(BUILDDIR)/$(LIBTARGET)

.PHONY: clean
clean:
	rm -rf build*
