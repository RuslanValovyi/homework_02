Hello everyone!

Please, find below a homework task.

Create a very small project with three source files: one header file and two source files (if you want you can create more files).
Feel free to use source code from [the following example](https://github.com/SergiiPiatakov/calculator).
Create a `Makefile` and `CMakeLists.txt` to build your project. The build result should be the following: one library (static or shared) and one executable.
Generate from the `CMakeLists.txt` file a build script for `Ninja` build system.

Please, provide me with:
- link on your repo (it must contain sources and both `Makefile` and `CMakeLists.txt`);
- the generated build script for Ninja build system.

In case of any difficulties don't hesitate to ask for help.

Best Regards,
Sergii.
